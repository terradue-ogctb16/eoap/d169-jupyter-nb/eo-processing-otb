FROM terradue/l0-binder:latest

MAINTAINER Terradue S.r.l

COPY . ${HOME}
USER root

RUN /opt/anaconda/bin/conda env create --file ${HOME}/environment.yml

RUN /opt/anaconda/envs/env_eoap/bin/python -m ipykernel install --name env_eoap

RUN chown -R ${NB_UID} ${HOME}
RUN mkdir -p /workspace/data && chown -R ${NB_UID} /workspace/data
USER ${NB_USER}

#RUN source /opt/anaconda/envs/env_eoap/etc/conda/activate.d/otb_env_activate.sh
RUN opensearch-client -p do=terradue -p start='2018-12-29T00:00:00Z' -p stop='2018-12-29T23:59:59Z' -p geom='POLYGON ((15.399 37.348, 15.399 38.148, 14.599 38.148, 14.599 37.348, 15.399 37.348))' -p pt=S2MSI2A https://catalog.terradue.com/sentinel2/description enclosure | ciop-copy -O /workspace/data -

WORKDIR ${HOME}